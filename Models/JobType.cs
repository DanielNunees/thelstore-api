﻿namespace lawstore.Models
{
    public class JobType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}

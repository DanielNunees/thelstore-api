﻿using System;

namespace lawstore.Models
{
    public class Jobs
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public JobType JobType { get; set; }
        public int JobTypeId { get; set; }
        public decimal Salary { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
    }
}

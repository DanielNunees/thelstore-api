﻿using lawstore.Factories;
using lawstore.Mapping;
using Microsoft.EntityFrameworkCore;
namespace lawstore.Models
{
    public class JobsContext : DbContext
    {
        public JobsContext(DbContextOptions<JobsContext> options)
            : base(options)
        {
        }
        public DbSet<Jobs> Jobs { get; set; }
        public DbSet<JobType> JobType { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(JobsEntityTypeConfiguration).Assembly);

            //Create Job types
            // create(max = 3)
            var jobTypes = new JobTypeSeeder().create(3);
            modelBuilder.Entity<JobType>().HasData(jobTypes);

            // Create fake data for Jobs table
            var jobs = new JobsSeeder().create(10);
            modelBuilder.Entity<Jobs>().HasData(jobs);

        }

    }
}

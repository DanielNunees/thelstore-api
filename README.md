﻿## Api created to show jobs positions from the database

## Run project

### Change the connectionString

Inside the `appsettings.json` file, a connection string to the dabase is set up, change with yours credentials
```json
"ConnectionStrings": {
	"DefaultConnection": "Server=localhost;Database=lawstore;User Id=sa;Password=MSSQLrootpasword@1433;"
}
```

### Create the database and run the migration

If your databse it is not created run `dotnet ef database update`
this command will create the database and run the migrations

### Run the project

On Windows computer press `ctr+f5` to execute the project
On MacOS pres `cmd+f5` to execute the project

### Port

The project will run at the port 5000/5001, make sure that the ports are available.
And the end of the build the project will open the browser in a pre determined route showing some data.
If this doesn't happen, check your string connection and ports, and try again.
Please call the responsible developer if the problems persist.

## Available Scripts

In the project directory, you can run:

### `dotnet ef database drop`

Drops the database all its tables and data .

### `dotnet ef migrations remove`

Remove all migrations created for the database

### `dotnet ef migrations add InitialMigration`

Creates a new migration with the current models modifications

### `dotnet ef database update`

Runs the migations and refreshes the database


## Project Routes

### Get all jobs
GET `https://localhost:5001/api/jobs`

### Create a new Job
GET `https://localhost:5001/api/jobs/search?searchString={text}`

	 {
		"Title": "Software developer",
		"Description": "Software developer",
		"Salary": 72000,
		"JobTypeId": 1
	 }

### Delete a job
DELETE `https://localhost:5001/api/jobs/{id}`

### Search by Title
GET `https://localhost:5001/api/jobs/search?searchString={text}`

### Update a job
PUT `https://localhost:5001/api/jobs/{id}`
	{
		"id":5,
		"Title": "Software developer",
		"Description": "Software developer",
		"Salary": 72000,
		"JobTypeId": 1
	}



## Dependencies [5]

- Bogus [https://github.com/bchavez/Bogus]
Fake data package

- .NETCore 3.1.0 [https://dotnet.microsoft.com/download/dotnet/3.1]
.NET Core is a runtime to execute applications build on it.

- .AspNetCore 3.1.10 [https://dotnet.microsoft.com/download/dotnet/3.1]
ASP.NET Core is a web framework to build web apps, IoT apps, and mobile backends on the top of .NET Core or .NET Framework.

- EntityFrameworkCore 5.0.7 [https://www.nuget.org/packages/Microsoft.EntityFrameworkCore]
Entity Framework Core is a modern object-database mapper for .NET. It supports LINQ queries, change tracking, updates, and schema migrations.


### Created by Daniel Nunes
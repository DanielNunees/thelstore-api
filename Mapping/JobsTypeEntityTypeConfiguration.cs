﻿using lawstore.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace lawstore.Mapping
{
    public class JobsTypeEntityTypeConfiguration : IEntityTypeConfiguration<JobType>
    {
        public void Configure(EntityTypeBuilder<JobType> builder)
        {
            builder.Property(e => e.Type).IsRequired().HasMaxLength(50);
        }
    }
}

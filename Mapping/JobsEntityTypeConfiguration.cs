﻿using System;
using lawstore.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace lawstore.Mapping
{
    public class JobsEntityTypeConfiguration : IEntityTypeConfiguration<Jobs>
    {
        public void Configure(EntityTypeBuilder<Jobs> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasOne(e => e.JobType).WithMany();
            builder.Property(e => e.Title).IsRequired().HasMaxLength(50);
            builder.Property(e => e.Description).IsRequired();
            builder.Property(e => e.Created).HasDefaultValue(DateTime.Now);
            builder.Property(e => e.Salary).HasColumnType("decimal(7,2)");
        }
    }
}

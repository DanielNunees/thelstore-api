using System.Collections.Generic;
using System.Linq;
using lawstore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace lawstore.Controllers
{
    /// <summary>
    /// The main Jobs class.
    /// Contains all methods for performing basic CRUD actions over
    /// the table jobs in the database.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class JobsController : ControllerBase
    {
        private readonly JobsContext _context;

        public JobsController(JobsContext context) => _context = context;

        /// <summary>
        /// Retrieve all the data from the jobs table
        /// GET: api/jobs/
        /// </summary>
        [HttpGet]
        public IEnumerable<Jobs> GetAll()
        {
            var jobs = _context.Jobs.Include(jobs => jobs.JobType).ToList();
            return jobs;
        }

        /// <summary>
        /// Delete a existing job
        /// DELETE: api/jobs/5
        /// </summary>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var jobFound = _context.Jobs.Find(id);
            if (jobFound == null)
            {
                return NotFound();
            }

            _context.Jobs.Remove(jobFound);
            _context.SaveChanges();

            return Ok();
        }

        /// <summary>
        /// Search for a job by title
        /// GET: api/jobs/search?searchString=
        /// </summary>
        [HttpGet("{search}")]
        public IEnumerable<Jobs> Search(string searchString)
        {
            if (!string.IsNullOrEmpty(searchString))
            {
                var items = _context.Jobs.Where(
                job => job.Title.Contains(searchString))
                    .Include(job => job.JobType)
                .ToList();
                return items;
            }
            else
            {
                var jobs = _context.Jobs.Include(jobs => jobs.JobType).ToList();
                return jobs;
            }
        }

        /// <summary>
        /// Create a new a job position
        /// POST: api/jobs
        /// </summary>
        [HttpPost]
        public IActionResult CreateJob(Jobs job)
        {
            if (job != null)
            {
                _context.Jobs.Add(job);
                _context.SaveChanges();
                return Ok(job);
            }
            return BadRequest();

        }

        /// <summary>
        /// Update a job position
        /// PUT: api/jobs
        /// </summary>
        [HttpPut("{id}")]
        public IActionResult Update(int id, Jobs job)
        {
            if (!id.Equals(job.Id))
            {
                return BadRequest();
            }
            _context.Entry(job).State = EntityState.Modified;
            var jobFound = _context.Jobs.Find(id);
            if (jobFound is null)
            {
                return NotFound();
            }
            _context.SaveChanges();
            return Ok();
        }

    }
}
﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace lawstore.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "JobType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Jobs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    JobTypeId = table.Column<int>(type: "int", nullable: false),
                    Salary = table.Column<decimal>(type: "decimal(7,2)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2021, 6, 16, 9, 22, 49, 550, DateTimeKind.Local).AddTicks(2660))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jobs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Jobs_JobType_JobTypeId",
                        column: x => x.JobTypeId,
                        principalTable: "JobType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "JobType",
                columns: new[] { "Id", "Type" },
                values: new object[] { 1, "Frontend" });

            migrationBuilder.InsertData(
                table: "JobType",
                columns: new[] { "Id", "Type" },
                values: new object[] { 2, "Backend" });

            migrationBuilder.InsertData(
                table: "JobType",
                columns: new[] { "Id", "Type" },
                values: new object[] { 3, "Fullstack" });

            migrationBuilder.InsertData(
                table: "Jobs",
                columns: new[] { "Id", "Created", "Description", "JobTypeId", "Salary", "Title" },
                values: new object[,]
                {
                    { 9, new DateTime(2021, 4, 23, 13, 29, 48, 484, DateTimeKind.Local).AddTicks(482), "Chief\n Accusamus distinctio suscipit facere eaque molestias sed nemo qui. Aliquam commodi voluptatibus tenetur perferendis error maiores. Placeat voluptatem non voluptates repudiandae non accusantium consequatur exercitationem. Officiis magni voluptate voluptas.\n\nEt quas quisquam. Rerum fuga aut. Nulla et deserunt doloremque tenetur.", 1, 71612m, "International Division Designer" },
                    { 3, new DateTime(2020, 9, 1, 12, 15, 48, 956, DateTimeKind.Local).AddTicks(7011), "Customer\n Labore nam quae. Voluptatem quia sit velit rerum commodi autem. Nam voluptatem qui quod impedit tenetur rerum sit tempore voluptatem. Totam porro dolores unde perspiciatis porro et quo. Non iusto cumque itaque repellendus.\n\nQuod sed non cupiditate vel voluptate autem. Veniam aut est. Voluptatem hic atque qui eius ut in consequatur.", 2, 78730m, "Regional Identity Manager" },
                    { 5, new DateTime(2020, 11, 10, 22, 14, 20, 182, DateTimeKind.Local).AddTicks(917), "Chief\n Dolores ea sint libero non eius doloremque est velit id. Sunt non earum inventore iusto itaque accusantium et velit consequatur. Ut aspernatur voluptates similique harum.\n\nCorrupti et tempore et nulla. Quis quia a dolorem dolor nostrum. Eos qui et similique et omnis. Ut quia et iusto repellendus aut labore ipsum quam omnis.", 2, 77554m, "Forward Functionality Engineer" },
                    { 6, new DateTime(2020, 10, 31, 22, 42, 53, 277, DateTimeKind.Local).AddTicks(8706), "Future\n Natus assumenda aut reiciendis velit. Quia nihil sed explicabo quia. Repudiandae vitae unde hic dolorem.\n\nQuia quod ratione sed. Velit laudantium mollitia optio beatae molestiae rem nam eum nostrum. Fugit ut nisi quae fugit nam soluta quia architecto dolore. Ducimus et ab ea perspiciatis nulla. Veritatis dolorum esse. Quia dolor minus est molestiae nisi est fuga.", 2, 88816m, "Product Assurance Technician" },
                    { 7, new DateTime(2020, 12, 31, 9, 37, 9, 83, DateTimeKind.Local).AddTicks(5277), "Internal\n Blanditiis veritatis voluptatem. Expedita repellendus sit. Est quisquam aut. Rerum quia libero in laudantium accusantium error. Iure labore laboriosam et qui. Blanditiis dolorem voluptate et rerum quisquam quasi eveniet.\n\nReprehenderit est omnis asperiores porro harum dolor. Aut repellat accusamus quidem tenetur sint ut. Voluptatem laudantium id eius est officia deserunt et. Consectetur nobis doloremque beatae neque. Maxime porro odit sit at voluptatum similique.", 2, 82720m, "Direct Group Developer" },
                    { 10, new DateTime(2020, 9, 28, 20, 1, 45, 680, DateTimeKind.Local).AddTicks(3969), "Lead\n Placeat consequatur est aperiam non et et voluptatem velit molestiae. A aperiam excepturi nulla eveniet. Consequuntur sequi numquam et quis recusandae asperiores illo quae placeat.\n\nQuisquam sed aut autem sapiente possimus voluptates commodi. Velit ipsa aut cupiditate molestiae nam est repellat numquam debitis. Iste omnis vitae pariatur sit. Praesentium officiis minus reiciendis dolorem ipsam nihil facilis deleniti aut. Neque quasi facere recusandae.", 2, 89950m, "Central Quality Manager" },
                    { 1, new DateTime(2021, 2, 18, 15, 57, 39, 833, DateTimeKind.Local).AddTicks(8532), "Regional\n Dolorem odio neque velit dolores pariatur minus. Excepturi esse in est delectus dicta veniam consequatur. Vero qui consequatur nostrum totam saepe. Dolorem dolores sequi quam voluptas tempore.\n\nDolores pariatur perspiciatis id quod qui quasi nam et nihil. Id voluptatibus harum architecto voluptatem cupiditate est aliquid. Culpa repellat corporis ex perspiciatis libero et commodi eligendi ipsam. Quo dolorem blanditiis rem sed ut soluta et et. Nesciunt praesentium officia illum sequi excepturi sed veritatis necessitatibus.", 3, 62101m, "Corporate Data Consultant" },
                    { 2, new DateTime(2020, 8, 17, 12, 59, 52, 223, DateTimeKind.Local).AddTicks(9376), "Customer\n Quasi in impedit. Deserunt sint consequatur repudiandae non neque a quisquam voluptas ratione. Voluptatem nihil sed porro sed iure nesciunt impedit.\n\nEsse porro sint ipsum sunt sit similique ex fuga. Illo beatae amet accusamus libero aut labore. Autem aut dolorem illo molestiae animi quas itaque id eum. Libero repudiandae accusamus voluptate dolorem quia velit nostrum.", 3, 73088m, "Dynamic Identity Liaison" },
                    { 4, new DateTime(2020, 7, 6, 16, 10, 18, 803, DateTimeKind.Local).AddTicks(5792), "Central\n Qui temporibus tenetur deserunt. Eveniet est laudantium dolores aut veritatis. Esse voluptatem dignissimos quasi ratione. Iure dignissimos eaque atque delectus quia enim. Sed maxime qui est quis hic reprehenderit pariatur fugiat. Eligendi veritatis eum ab quo.\n\nSunt sint totam labore ea ut voluptate eligendi. Quasi ut adipisci ut qui placeat minima similique dolor ut. Consequatur mollitia aut hic similique et consequuntur vel. Placeat suscipit est. Omnis debitis est. Et possimus in voluptatem repellat laboriosam debitis at expedita.", 3, 74799m, "Central Creative Designer" },
                    { 8, new DateTime(2021, 1, 20, 9, 8, 51, 562, DateTimeKind.Local).AddTicks(7230), "Human\n Necessitatibus est hic praesentium est neque eum quo et. Harum nam cupiditate. Placeat perspiciatis facilis dignissimos quia et nulla sit fugit numquam. Et reiciendis omnis aut delectus eos illo sint. Autem vel consequuntur nihil rerum. Suscipit enim eum fugiat voluptas et architecto.\n\nIpsum voluptatibus consequatur quod saepe accusantium natus sed. Assumenda maxime sit dolorum eum quo aut in. Quia voluptatem quis esse. Et ut officia in hic veniam quis repellendus temporibus dolorem. Voluptas voluptatibus magni. Dolorum exercitationem ut nulla reprehenderit.", 3, 66078m, "International Web Architect" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_JobTypeId",
                table: "Jobs",
                column: "JobTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Jobs");

            migrationBuilder.DropTable(
                name: "JobType");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using lawstore.Models;

namespace lawstore.Factories
{
    public class JobTypeSeeder : ISeed<JobType>
    {
        public IEnumerable<JobType> create(int quantity)
        {
            string[] JobTypesNames = { "Frontend", "Backend", "Fullstack" };
            var jobTypes = new List<JobType>();
            int qty = JobTypesNames.Length > quantity ? quantity : JobTypesNames.Length;

            var id = 1;
            for (int i = 0; i < qty; i++)
            {
                jobTypes.Add(new JobType { Id = id++, Type = JobTypesNames[i] });
            }
            return jobTypes;
        }

    }
}

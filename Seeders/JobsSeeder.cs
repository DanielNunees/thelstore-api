﻿using System;
using System.Collections.Generic;
using lawstore.Models;

namespace lawstore.Factories
{
    public class JobsSeeder : ISeed<Jobs>
    {

        public IEnumerable<Jobs> create(int quantity)
        {
            var id = 1;
            var jobs = new Bogus.Faker<Jobs>()
                .RuleFor(m => m.Id, f => id++)
                .RuleFor(m => m.Title, f => f.Name.JobTitle())
                .RuleFor(m => m.Description, f => f.Name.JobDescriptor() + "\n " + f.Lorem.Paragraphs(2))
                .RuleFor(m => m.JobTypeId, f => f.Random.Int(1, 3))
                .RuleFor(m => m.Salary, f => f.Finance.Amount(60000, 90000, 0))
                .RuleFor(m => m.Created, f => f.Date.Past(1));

            return jobs.Generate(quantity);
        }
    }
}

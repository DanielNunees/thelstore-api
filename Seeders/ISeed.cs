﻿using System.Collections.Generic;
using lawstore.Models;

namespace lawstore.Factories
{
    public interface ISeed<T>
    {
        IEnumerable<T> create(int quantity);
    }
}
